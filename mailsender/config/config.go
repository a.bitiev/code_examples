package config

import (
	"fmt"

	"github.com/ilyakaznacheev/cleanenv"
)

type Config struct {
	Http    Http    `yaml:"http"`
	Mongo   Mongo   `yaml:"mongo"`
	Profile Profile `yaml:"profile"`
	Secure  Secure  `yaml:"secure"`
	Smtp    Smtp    `yaml:"smtp"`
}

type Http struct {
	Host     string `yaml:"host" env:"SERVER_HOST" env-default:"localhost" env-description:"Server host"`
	Port     string `yaml:"port" env:"SERVER_PORT" env-default:"8084" env-description:"Server port"`
	CertFile string `yaml:"cert_file" env:"CERT_FILE" env-description:"Path to TLS certificate"`
	KeyFile  string `yaml:"key_file" env:"KEY_FILE" env-description:"Path to TLS key"`
}

type Mongo struct {
	URI        string `yaml:"uri" env:"MONGO_URI" env-default:"localhost" env-description:"MongoDB URI"`
	Database   string `yaml:"database" env:"MONGO_DATABASE" env-default:"email" env-description:"MongoDB database name"`
	Collection string `yaml:"collection" env:"MONGO_COLLECTION" env-default:"queue" env-description:"MongoDB collection name"`
}

type Profile struct {
	Host string `yaml:"host" env:"PROFILE_HOST" env-default:"localhost" env-description:"Profile server host"`
	Port string `yaml:"port" env:"PROFILE_PORT" env-default:"8081" env-description:"Profile server port"`
}

type Secure struct {
	SecretKey string `env:"SECRET_KEY" env-description:"Secret key for signing JSON Web Token"`
	HashAlgo  string `env:"HASH_ALGO" env-description:"Password hashing algorithm"`
}

type Smtp struct {
	Username string `env:"SMTP_USERNAME" env-description:"Username to authenticate on smtp server"`
	Password string `env:"SMTP_PASSWORD" env-description:"Password to authenticate on smtp server"`
	Host     string `yaml:"host" env:"SMTP_HOST" env-default:"smtp.mailtrap.io" env-description:"Smtp server host"`
	Port     string `yaml:"port" env:"SMTP_PORT" env-default:"2525" env-description:"Smtp server port"`
}

func Parse() (*Config, error) {
	var cfg Config
	var header string = "Configuration info"

	_, err := cleanenv.GetDescription(&cfg, &header)
	if err != nil {
		return nil, fmt.Errorf("failed to get description: %s", err)
	}

	// TODO config path from CLI arguments
	if err := cleanenv.ReadConfig("config.yaml", &cfg); err != nil {
		return nil, fmt.Errorf("failed to read config: %s", err)
	}

	return &cfg, nil
}
