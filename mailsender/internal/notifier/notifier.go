package notifier

type Notifier interface {
	Notify(string, string) error
}
