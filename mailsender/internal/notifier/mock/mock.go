package mock

import "gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"

type NotifierMock struct{}

func New(cfg *config.Profile) *NotifierMock {
	return &NotifierMock{}
}

func (n *NotifierMock) Notify(id, status string) error {
	return nil
}
