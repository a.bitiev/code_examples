package profile

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
)

type Notification struct {
	Id     string `json:"id"`
	Status string `json:"status"`
}

type ProfileClient struct {
	host   string
	port   string
	client *http.Client
}

func New(cfg *config.Profile) *ProfileClient {
	return &ProfileClient{
		host:   cfg.Host,
		port:   cfg.Port,
		client: &http.Client{},
	}
}

func (p *ProfileClient) Notify(id, status string) error {
	data, err := json.Marshal(Notification{Id: id, Status: status})
	if err != nil {
		return err
	}

	reader := bytes.NewReader(data)

	_, err = http.Post("http://"+p.host+":"+p.port, "application/json", reader)
	if err != nil {
		return err
	}

	return nil
}
