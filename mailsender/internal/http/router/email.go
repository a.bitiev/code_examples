package router

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/errors"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
)

func (u *UserRouter) emails() http.Handler {
	r := chi.NewRouter()
	r.Post("/", u.PutIntoQueue)

	return r
}

func (u *UserRouter) PutIntoQueue(w http.ResponseWriter, r *http.Request) {
	email := new(model.Email)

	data, err := io.ReadAll(r.Body)
	if err != nil {
		handleError(w, r, fmt.Errorf("%w: %s", errors.ErrBadRequest, errors.ErrBadJSON))
		return
	}

	err = json.Unmarshal(data, &email)
	if err != nil {
		handleError(w, r, fmt.Errorf("%w: %s", errors.ErrBadRequest, errors.ErrBadJSON))
		return
	}

	status, err := u.app.PutIntoQueue(r.Context(), email)
	if err != nil {
		handleError(w, r, fmt.Errorf("%w: %s", errors.ErrBadRequest, err))
		return
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, model.Responce{
		Id:     email.Id,
		Status: status,
	})
}
