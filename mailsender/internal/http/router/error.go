package router

import (
	"errors"
	"net/http"

	"github.com/go-chi/render"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
	myerr "gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/errors"
)

type Error struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
}

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	var (
		code int
		msg  string
	)

	switch {
	case errors.Is(err, auth.ErrUnauthorized):
		code = http.StatusUnauthorized
		msg = err.Error()
	case errors.Is(err, myerr.ErrForbidden):
		code = http.StatusForbidden
		msg = err.Error()
	case errors.Is(err, myerr.ErrBadRequest):
		code = http.StatusBadRequest
		msg = err.Error()
	case errors.Is(err, myerr.ErrUnprocessableEntity):
		code = http.StatusUnprocessableEntity
		msg = err.Error()
	case errors.Is(err, myerr.ErrExists):
		code = http.StatusConflict
		msg = err.Error()
	case errors.Is(err, myerr.ErrNotFound):
		code = http.StatusNotFound
		msg = err.Error()
	default:
		code = http.StatusInternalServerError
		msg = myerr.ErrInternal.Error()
	}

	render.Status(r, code)
	render.JSON(w, r, &Error{
		Code:  code,
		Error: msg,
	})
}

func notFound(w http.ResponseWriter, r *http.Request) {
	render.Status(r, http.StatusNotFound)
	render.JSON(w, r, &Error{
		Code:  http.StatusNotFound,
		Error: myerr.ErrNotFound.Error(),
	})
}
