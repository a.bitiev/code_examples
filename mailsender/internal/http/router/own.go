package router

import (
	"net/http"

	"github.com/go-chi/chi"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/errors"
)

func OnlyOwn(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, err := userFromContext(r)
		if err != nil {
			handleError(w, r, err)
			return
		}

		userID := user.ID
		pathID := chi.URLParam(r, "user_id")

		if userID != pathID {
			handleError(w, r, errors.ErrForbidden)
			return
		}

		next.ServeHTTP(w, r)
	})
}
