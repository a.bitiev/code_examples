package router

import (
	"net/http"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/mailsender"
)

type UserRouter struct {
	auth auth.Authenticator
	app  *mailsender.Mailsender
}

func NewUserRouter(cfg *config.Secure, app *mailsender.Mailsender) (*UserRouter, error) {
	auth, err := auth.NewAuthenticator("jwt", []byte(cfg.SecretKey))
	if err != nil {
		return nil, err
	}

	return &UserRouter{
		auth: auth,
		app:  app,
	}, nil
}

func (u *UserRouter) Routes() http.Handler {
	r := chi.NewRouter()

	r.Use(
		middleware.NoCache,
		middleware.Logger,
		middleware.Recoverer,
		middleware.RealIP,
		render.SetContentType(render.ContentTypeJSON),
		u.Authentication,
	)

	r.Mount("/emails", u.emails())
	return r
}
