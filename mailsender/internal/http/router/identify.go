package router

import (
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
)

type UserInfo struct {
	Login string `json:"login"`
}

func (u *UserRouter) identification(path string) http.Handler {
	r := chi.NewRouter()

	r.Route(path, func(r chi.Router) {
		r.Get("/", u.identify)
	})

	return r
}

func (u *UserRouter) identify(w http.ResponseWriter, r *http.Request) {
	user, err := userFromContext(r)
	if err != nil {
		handleError(w, r, err)
		return
	}

	resp := &UserInfo{
		Login: user.Login,
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, resp)
}
