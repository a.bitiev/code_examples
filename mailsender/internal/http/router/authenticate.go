package router

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

func (u *UserRouter) Authentication(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, authenticated := u.authenticate(r, auth.AccessKey)
		if authenticated {
			ctx := context.WithValue(r.Context(), auth.UserKey, user)
			next.ServeHTTP(w, r.WithContext(ctx))
			return
		}

		comeback := fmt.Sprintf("/auth/me?redirect_uri=%s", r.RequestURI)

		http.Redirect(w, r, comeback, http.StatusTemporaryRedirect)
	})
}

func (u *UserRouter) authenticate(r *http.Request, tokenKey string) (*users.User, bool) {
	cookie, err := r.Cookie(tokenKey)
	if err != nil {
		return nil, false
	}

	if cookie.Expires.After(time.Now().UTC()) {
		return nil, false
	}

	user, err := u.auth.ParseToken(cookie.Value)
	if err != nil {
		return nil, false
	}

	return user, true
}

func userFromContext(r *http.Request) (*users.User, error) {
	value := r.Context().Value(auth.UserKey)

	user, ok := value.(*users.User)
	if !ok {
		return nil, auth.ErrUnauthorized
	}

	return user, nil
}
