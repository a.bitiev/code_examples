package http

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/http/router"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/mailsender"
)

var (
	ReadTimeout  = 15 * time.Second
	WriteTimeout = 15 * time.Second
)

type Server struct {
	server   *http.Server
	certFile string
	keyFile  string
}

func New(cfg *config.Config, app *mailsender.Mailsender) (*Server, error) {
	r, err := router.NewUserRouter(&cfg.Secure, app)
	if err != nil {
		return nil, err
	}

	srv := NewServer(&cfg.Http, r)

	return srv, nil
}

func NewServer(cfg *config.Http, r *router.UserRouter) *Server {
	return &Server{
		server: &http.Server{
			Addr:         cfg.Host + ":" + cfg.Port,
			Handler:      r.Routes(),
			ReadTimeout:  ReadTimeout,
			WriteTimeout: WriteTimeout,
		},
		certFile: cfg.CertFile,
		keyFile:  cfg.KeyFile,
	}
}

func (s *Server) Run() error {
	var err error

	if s.certFile == "" || s.keyFile == "" {
		err = s.server.ListenAndServe()
	} else {
		err = s.server.ListenAndServeTLS(s.certFile, s.keyFile)
	}

	if err != nil && err != http.ErrServerClosed {
		return err
	}

	return nil
}

func (s *Server) Stop() error {
	return s.server.Shutdown(context.Background())
}
