package sender

import "gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"

type Sender interface {
	Send(email *model.Email) error
	Close() error
}
