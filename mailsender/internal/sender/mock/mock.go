package mock

import (
	"fmt"
	"math/rand"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
)

type SenderMock struct{}

func New(cfg *config.Smtp) *SenderMock {
	rand.Seed(time.Now().Unix())
	return &SenderMock{}
}

func (s *SenderMock) Send(email *model.Email) error {
	fmt.Printf("sended mail from %s to %s\n", email.From, email.To)
	return nil
}

func (s *SenderMock) Close() error {
	return nil
}
