package smtp

import (
	"crypto/tls"
	"fmt"
	"net/smtp"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
)

type StmpClient struct {
	client *smtp.Client
}

func New(cfg *config.Smtp) (*StmpClient, error) {
	tlsConfig := &tls.Config{
		ServerName: cfg.Host,
	}

	auth := smtp.PlainAuth("", cfg.Username, cfg.Password, cfg.Host)
	server := cfg.Host + ":" + cfg.Port

	client, err := smtp.Dial(server)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to %s server: %s", server, err)
	}

	err = client.StartTLS(tlsConfig)
	if err != nil {
		return nil, fmt.Errorf("failed to establish TLS connection: %s", err)
	}

	if err = client.Auth(auth); err != nil {
		return nil, fmt.Errorf("failed to authenticate smtp client: %s", err)
	}

	return &StmpClient{
		client: client,
	}, nil
}

func (c *StmpClient) Send(email model.Email) error {
	if err := c.client.Mail(email.From); err != nil {
		return err
	}

	if err := c.client.Rcpt(email.From); err != nil {
		return err
	}

	w, err := c.client.Data()
	if err != nil {
		return err
	}
	defer w.Close()

	if _, err = w.Write(email.Build()); err != nil {
		return err
	}

	return nil
}

func (c *StmpClient) Close() error {
	return c.client.Close() // or Quit()
}
