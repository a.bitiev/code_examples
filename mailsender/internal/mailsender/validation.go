package mailsender

import (
	"fmt"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/errors"
)

type Validator interface {
	Validate() error
}

func validate(v Validator) error {
	if err := v.Validate(); err != nil {
		return fmt.Errorf("%w: %v", errors.ErrUnprocessableEntity, err)
	}

	return nil
}
