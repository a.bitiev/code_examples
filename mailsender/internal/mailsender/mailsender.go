package mailsender

import (
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/queue"
	"go.uber.org/zap"
)

const (
	name = "Mailsender"
)

type Mailsender struct {
	emailQueue queue.EmailQueue
	notifyChan chan struct{}
	logger     *zap.SugaredLogger
}

func New(logger *zap.SugaredLogger, queue queue.EmailQueue, channel chan struct{}) *Mailsender {
	return &Mailsender{
		emailQueue: queue,
		logger:     logger,
		notifyChan: channel,
	}
}
