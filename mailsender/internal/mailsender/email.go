package mailsender

import (
	"context"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
)

func (m *Mailsender) PutIntoQueue(ctx context.Context, email *model.Email) (string, error) {
	if err := validate(email); err != nil {
		return "", err
	}

	status, err := m.emailQueue.Put(ctx, email)
	if err != nil {
		return "", err
	}

	m.notifyChan <- struct{}{}

	return status, nil
}
