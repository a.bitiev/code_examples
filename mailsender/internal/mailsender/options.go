package mailsender

import (
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/queue/mongo"
)

type Option func(*Mailsender)

func WithEmailQueue(cfg *config.Config) (Option, error) {
	emailQueue, err := mongo.New(&cfg.Mongo)
	if err != nil {
		return nil, err
	}

	return func(m *Mailsender) {
		m.emailQueue = emailQueue
	}, nil
}

func (m *Mailsender) Name() string {
	return name
}
