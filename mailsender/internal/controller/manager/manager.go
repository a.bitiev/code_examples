package manager

import (
	"context"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/notifier"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/queue"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/sender"
	"go.uber.org/zap"
)

var (
	timeout = 5 * time.Second
)

type Manager struct {
	emailQueue queue.EmailQueue
	sender     sender.Sender
	notifier   notifier.Notifier
	logger     *zap.SugaredLogger
	notifyChan chan struct{}
	done       chan struct{}
	stop       chan struct{}
}

func New(eq queue.EmailQueue, s sender.Sender, n notifier.Notifier, logger *zap.SugaredLogger, c chan struct{}) *Manager {
	return &Manager{
		emailQueue: eq,
		sender:     s,
		notifier:   n,
		logger:     logger,
		notifyChan: c,
		done:       make(chan struct{}),
		stop:       make(chan struct{}),
	}
}

func (m *Manager) Listen() {
	for {
		select {
		case <-m.notifyChan:
			m.Manage()
		case <-m.stop:
			m.done <- struct{}{}
			return
		}
	}
}

func (m *Manager) Manage() {
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	email, err := m.emailQueue.Get(ctx)
	if err != nil {
		return
	}

	for email.Attempts < 3 {
		if err := m.sender.Send(email); err == nil {
			break
		}
		email.Attempts++
	}

	if err == nil {
		err = m.emailQueue.Update(context.Background(), email.Id, model.StatusSend)
		if err != nil {
			m.handleError(email, err)
		}
		err = m.notifier.Notify(email.Id, model.StatusSend)
		if err != nil {
			m.handleError(email, err)
		}
	} else {
		err = m.emailQueue.Update(context.Background(), email.Id, model.StatusError)
		if err != nil {
			m.handleError(email, err)
		}
		err = m.notifier.Notify(email.Id, model.StatusError)
		if err != nil {
			m.handleError(email, err)
		}
	}
}

func (m *Manager) Close() error {
	m.stop <- struct{}{}
	<-m.done
	close(m.stop)
	close(m.done)
	return nil
}

func (m *Manager) Notify() {
	m.notifyChan <- struct{}{}
}

func (m *Manager) handleError(email *model.Email, err error) {
	m.notifyChan <- struct{}{}
	m.logger.Error(err)
}
