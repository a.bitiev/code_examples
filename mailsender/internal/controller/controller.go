package controller

type Controller interface {
	Manage()
	Notify()
	Listen()
	Close() error
}
