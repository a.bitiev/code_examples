package model

import (
	"errors"
	"fmt"
	"net/mail"
)

const (
	StatusPending = "pending"
	StatusSend    = "send"
	StatusError   = "error"
)

type Email struct {
	Id      string `bson:"_id" json:"id"`
	From    string `bson:"from" json:"from"`
	To      string `bson:"to" json:"to"`
	Subject string `bson:"subject" json:"subject"`
	Body    string `bson:"body" json:"body"`

	Status   string `bson:"status" json:"status"`
	Attempts int    `bson:"attempts" json:"attempts"`
	Stamp    int64  `bson:"stamp" json:"stamp"`
}

var (
	ErrEmptyEmail           = errors.New("empty email")
	ErrEmptyId              = errors.New("empty id")
	ErrEmptySender          = errors.New("empty sender")
	ErrEmptyReceiver        = errors.New("empty receiver")
	ErrEmptyBody            = errors.New("empty body")
	ErrInvalidSenderEmail   = errors.New("invalid sender email")
	ErrInvalidReceiverEmail = errors.New("invalid receiver email")
)

func (e *Email) Validate() error {
	switch {
	case e == nil:
		return ErrEmptyEmail
	case e.Id == "":
		return ErrEmptyId
	case e.From == "":
		return ErrEmptySender
	case e.To == "":
		return ErrEmptyReceiver
	case e.Body == "":
		return ErrEmptyBody
	}

	if _, err := mail.ParseAddress(e.From); err != nil {
		return ErrInvalidSenderEmail
	}

	if _, err := mail.ParseAddress(e.To); err != nil {
		return ErrInvalidReceiverEmail
	}

	return nil
}

func (e *Email) Build() []byte {
	res := ""
	res += fmt.Sprintf("From: %s\r\n", e.From)
	res += fmt.Sprintf("To: %s\r\n", e.To)
	res += fmt.Sprintf("Subject: %s\r\n", e.Subject)
	res += "\r\n" + e.Body

	return []byte(res)
}
