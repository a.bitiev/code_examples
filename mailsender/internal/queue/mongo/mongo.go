package mongo

import (
	"context"
	"errors"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	ErrNotInitialized = errors.New("mongodb not initialized")
	connectionTimeout = 10 * time.Second
)

type MongoQueue struct {
	client *mongo.Client
	queue  *mongo.Collection
}

func New(cfg *config.Mongo) (*MongoQueue, error) {
	ctx, cancel := context.WithTimeout(context.Background(), connectionTimeout)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.URI))
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		return nil, err
	}

	return &MongoQueue{
		client: client,
		queue:  client.Database(cfg.Database).Collection(cfg.Collection),
	}, nil
}

func (mq *MongoQueue) Ping(ctx context.Context) error {
	if mq == nil || mq.client == nil {
		return ErrNotInitialized
	}

	return mq.client.Ping(ctx, nil)
}

func (mq *MongoQueue) Close() error {
	return mq.client.Disconnect(context.TODO())
}
