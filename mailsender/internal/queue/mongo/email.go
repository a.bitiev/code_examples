package mongo

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	ErrNoDocumentsInQueue = errors.New("no documents in queue")
)

func (mq *MongoQueue) Name() string {
	return "mongo queue"
}

func (mq *MongoQueue) Put(ctx context.Context, email *model.Email) (string, error) {
	filter := bson.D{
		primitive.E{
			Key:   "_id",
			Value: email.Id,
		},
	}

	result := mq.queue.FindOne(ctx, filter)
	if result.Err() == nil {
		return "", fmt.Errorf("email with id=%s already in queue", email.Id)
	} else {
		if result.Err() != mongo.ErrNoDocuments {
			return "", fmt.Errorf("failed to find in queue email with id=%s", email.Id)
		}
	}

	email.Stamp = time.Now().UnixMilli()
	email.Status = model.StatusPending
	_, err := mq.queue.InsertOne(ctx, email)
	if err != nil {
		return "", fmt.Errorf("failed to insert document: %s", err)
	}

	return model.StatusPending, nil
}

func (mq *MongoQueue) Get(ctx context.Context) (*model.Email, error) {
	options := options.FindOne()
	options.SetSort(bson.D{
		bson.E{
			Key:   "stamp",
			Value: 1,
		},
	})

	filter := bson.M{"status": model.StatusPending}

	var email model.Email
	err := mq.queue.FindOne(ctx, filter, options).Decode(&email)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, ErrNoDocumentsInQueue
		}
		return nil, fmt.Errorf("failed to find document: %s", err)
	}

	return &email, nil
}

func (q *MongoQueue) Update(ctx context.Context, id, status string) error {

	filter := bson.M{"_id": id}

	var email model.Email

	update := bson.M{
		"$set": bson.M{"status": status},
	}

	err := q.queue.FindOneAndUpdate(ctx, filter, update).Decode(&email)
	if err != nil {
		return fmt.Errorf("failed to update email: %s", err)
	}

	return nil
}
