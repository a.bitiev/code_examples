package mock

import (
	"context"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
)

type EmailQueueMock struct{}

func New(cfg *config.Mongo) *EmailQueueMock {
	return &EmailQueueMock{}
}

func (q *EmailQueueMock) Put(ctx context.Context, email *model.Email) (string, error) {
	return model.StatusPending, nil
}

func (q *EmailQueueMock) Get(context.Context) (*model.Email, error) {
	return &model.Email{
		Id:      "1",
		From:    "test.user1@mst.ru",
		To:      "test.user2@mts.ru",
		Subject: "Testing",
		Body:    "Testing body",
	}, nil
}

func (q *EmailQueueMock) Update(context.Context, string, string) error {
	return nil
}

func (q *EmailQueueMock) Ping(context.Context) error {
	return nil
}

func (q *EmailQueueMock) Close() error {
	return nil
}
