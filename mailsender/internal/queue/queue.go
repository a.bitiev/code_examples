package queue

import (
	"context"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/model"
)

type EmailQueue interface {
	Put(context.Context, *model.Email) (string, error)
	Get(context.Context) (*model.Email, error)
	Update(context.Context, string, string) error
	Ping(context.Context) error
	Close() error
}
