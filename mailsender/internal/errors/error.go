package errors

import "errors"

var (
	ErrForbidden           = errors.New("forbidden")
	ErrBadRequest          = errors.New("bad request")
	ErrBadJSON             = errors.New("invalid json")
	ErrUnprocessableEntity = errors.New("unprocessable entity")
	ErrNotFound            = errors.New("not found")
	ErrExists              = errors.New("already exists")
	ErrInternal            = errors.New("internal error")
	ErrParse               = errors.New("parse error")
	ErrQueueInsert         = errors.New("queue insert error")
)
