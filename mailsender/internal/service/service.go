package service

import (
	"context"
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/controller"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/controller/manager"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/http"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/mailsender"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/notifier"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/notifier/profile"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/queue"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/queue/mongo"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/sender"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/sender/mock"
	"go.uber.org/zap"
)

type Service struct {
	mailsender *mailsender.Mailsender
	http       *http.Server
	logger     *zap.SugaredLogger
	manager    controller.Controller
	queue      queue.EmailQueue
	notifier   notifier.Notifier
	sender     sender.Sender
	notifyChan chan struct{}
}

func New(cfg *config.Config, logger *zap.SugaredLogger) (*Service, error) {

	emailQueue, err := mongo.New(&cfg.Mongo)
	if err != nil {
		return nil, fmt.Errorf("failed to create email queue: %s", err)
	}

	notifyChan := make(chan struct{})
	sender := mock.New(&cfg.Smtp)
	notifier := profile.New(&cfg.Profile)

	mailsender := mailsender.New(logger, emailQueue, notifyChan)
	if err != nil {
		return nil, fmt.Errorf("%s service init error: %w", mailsender.Name(), err)
	}

	manager := manager.New(emailQueue, sender, notifier, logger, notifyChan)

	httpServer, err := http.New(cfg, mailsender)
	if err != nil {
		return nil, fmt.Errorf("%s http server init error: %w", mailsender.Name(), err)
	}

	return &Service{
		mailsender: mailsender,
		manager:    manager,
		http:       httpServer,
		logger:     logger,
		queue:      emailQueue,
		notifier:   notifier,
		sender:     sender,
		notifyChan: notifyChan,
	}, nil
}

func (s *Service) Run() {
	s.logger.Infof("%s service started", s.mailsender.Name())

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go s.manager.Listen()

	go s.handleSignal(ctx, cancel)

	go func() {
		s.logger.Info("http server running...")
		if err := s.http.Run(); err != nil {
			s.logger.Errorf("failed to run server: %v", err)
			cancel()
		}
	}()

	<-ctx.Done()

	s.gracefulShutdown()

	s.logger.Infof("%s service stopped", s.mailsender.Name())
}

func (s *Service) gracefulShutdown() {
	var err error

	if err = s.http.Stop(); err != nil {
		s.logger.Infof("http server shutdown: %v", err)
	} else {
		s.logger.Info("http server shutdown: success")
	}

	s.Stop()
}

type Closer interface {
	Close() error
}

func (s *Service) Stop() {
	var err error

	internals := []Closer{
		s.queue,
		s.manager,
		s.sender,
	}

	for _, internal := range internals {
		if err = internal.Close(); err != nil {
			s.logger.Error(err)
		}
	}

	s.logger.Info("Services finished...")
	close(s.notifyChan)
	s.logger.Info("Notify channel closed...")
}

func (s *Service) handleSignal(ctx context.Context, cancel context.CancelFunc) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)

	select {
	case <-sigint:
		s.logger.Info("received SIGINT, shutting down...")
		cancel()
	case <-ctx.Done():
		return
	}
}
