package main

import (
	"log"

	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/mailsender/internal/service"
	"go.uber.org/zap"
)

func main() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("failed to initialize zap logger: %v", err)
	}
	defer logger.Sync()

	sugar := logger.Sugar()

	cfg, err := config.Parse()
	if err != nil {
		sugar.Fatal(err)
	}

	app, err := service.New(cfg, sugar)
	if err != nil {
		sugar.Fatal(err)
	}

	app.Run()
}
