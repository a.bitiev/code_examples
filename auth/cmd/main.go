package main

import (
	"log"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/service"
)

func main() {
	cfg, err := config.Parse()
	if err != nil {
		log.Fatal(err)
	}

	app, err := service.New(cfg)
	if err != nil {
		log.Fatal(err)
	}

	app.Run()
}
