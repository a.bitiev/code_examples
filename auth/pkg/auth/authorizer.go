package auth

import (
	"errors"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth/jwt"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

type ctxKey string

const (
	UserKey ctxKey = "user"

	AccessKey  = "access"
	RefreshKey = "refresh"

	AccessTime  = time.Minute
	RefreshTime = time.Hour
)

var ErrUnauthorized = errors.New("user is not authorized")

type Authorizer interface {
	NewToken(user *users.User, duration time.Duration) (string, error)
	ParseToken(tokenString string) (*users.User, error)
}

type Authenticator interface {
	ParseToken(tokenString string) (*users.User, error)
}

func NewAuthorizer(method string, key []byte) (Authorizer, error) {
	switch method {
	case jwt.Method:
		return jwt.New(key)
	default:
		return nil, errors.New("invalid authorization method")
	}
}

func NewAuthenticator(method string, key []byte) (Authenticator, error) {
	switch method {
	case jwt.Method:
		return jwt.New(key)
	default:
		return nil, errors.New("invalid authentication method")
	}
}
