package jwt

import (
	"errors"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v4"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

const Method = "jwt"

var (
	ErrInvalidKey    = errors.New("invalid jwt key")
	ErrInvalidClaims = errors.New("invalid claims")
)

type JWT struct {
	Key []byte
}

func New(key []byte) (*JWT, error) {
	if len(key) == 0 {
		return nil, ErrInvalidKey
	}

	return &JWT{Key: key}, nil
}

func (j *JWT) NewToken(user *users.User, duration time.Duration) (string, error) {
	claims := &Claims{
		ID:    user.ID,
		Login: user.Login,
		RegisteredClaims: &jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().UTC().Add(duration)),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	return token.SignedString(j.Key)
}

func (j *JWT) ParseToken(tokenString string) (*users.User, error) {
	tokenString = strings.TrimSpace(tokenString)

	token, err := jwt.ParseWithClaims(tokenString, &Claims{}, j.keyFunc)
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(*Claims)
	if !ok || !token.Valid {
		return nil, ErrInvalidClaims
	}

	return &users.User{
		ID:    claims.ID,
		Login: claims.Login,
	}, nil
}

func (j *JWT) keyFunc(*jwt.Token) (interface{}, error) {
	return j.Key, nil
}
