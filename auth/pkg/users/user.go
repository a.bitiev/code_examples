package users

import (
	"errors"

	"go.mongodb.org/mongo-driver/bson"
)

type User struct {
	ID       string `json:"id,omitempty"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

func (u *User) Validate() error {
	if u.Login == "" {
		return errors.New("login is empty")
	}

	if u.Password == "" {
		return errors.New("password is empty")
	}

	return nil
}

func (u *User) CreateDocument() bson.M {
	return bson.M{
		"login":    u.Login,
		"password": u.Password,
	}
}

func (u *User) UpdateDocument() bson.M {
	return bson.M{
		"$set": bson.M{
			"login":    u.Login,
			"password": u.Password,
		},
	}
}
