package router

import (
	"net/http"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

func (u *UserRouter) authenticate(r *http.Request, tokenKey string) (*users.User, bool) {
	cookie, err := r.Cookie(tokenKey)
	if err != nil {
		return nil, false
	}

	if cookie.Expires.After(time.Now().UTC()) {
		return nil, false
	}

	user, err := u.auth.ParseToken(cookie.Value)
	if err != nil {
		return nil, false
	}

	return user, true
}

func getUserCtx(r *http.Request) (*users.User, error) {
	value := r.Context().Value(auth.UserKey)

	user, ok := value.(*users.User)
	if !ok {
		return nil, auth.ErrUnauthorized
	}

	return user, nil
}
