package router

import (
	"log"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/authorization"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
)

type UserRouter struct {
	app  *authorization.Authorization
	auth auth.Authorizer
}

func NewClientRouter(cfg *config.Config, app *authorization.Authorization) (*UserRouter, error) {
	authorizer, err := auth.NewAuthorizer(cfg.Secure.Method, []byte(cfg.Secure.SecretKey))
	if err != nil {
		return nil, err
	}

	return &UserRouter{
		app:  app,
		auth: authorizer,
	}, nil
}

func (u *UserRouter) Routes() http.Handler {
	router := chi.NewRouter()

	router.Use(
		middleware.RequestID,
		middleware.RealIP,
		middleware.Logger,
		middleware.Recoverer,
	)

	router.Post("/login", u.login)
	router.Post("/logout", u.logout)

	router.Group(func(r chi.Router) {
		r.Use(u.reauthorize)
		r.Get("/i", u.identify)
		r.Get("/me", u.me)
	})

	router.Mount("/debug", debug())

	var urls []string
	for _, v := range router.Routes() {
		urls = append(urls, v.Pattern)
	}
	log.Print(strings.Join(urls, "\n"))

	return router
}
