package router

import (
	"net/http"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

func (u *UserRouter) login(w http.ResponseWriter, r *http.Request) {
	username, password, ok := r.BasicAuth()
	if !ok {
		handleError(w, r, auth.ErrUnauthorized)
		return
	}

	input := &users.User{
		Login:    username,
		Password: password,
	}

	user, err := u.app.Authorize(r.Context(), input)
	if err != nil {
		handleError(w, r, err)
		return
	}

	if err = u.authorize(w, user); err != nil {
		handleError(w, r, err)
		return
	}

	url := r.URL.Query().Get("redirect_uri")
	if url != "" {
		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
		return
	}

	w.WriteHeader(http.StatusOK)
}
