package router

import (
	"net/http"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
)

func (u *UserRouter) logout(w http.ResponseWriter, r *http.Request) {
	http.SetCookie(w, resetCookie(auth.AccessKey))
	http.SetCookie(w, resetCookie(auth.RefreshKey))

	url := r.URL.Query().Get("redirect_uri")
	if url != "" {
		http.Redirect(w, r, url, http.StatusTemporaryRedirect)
		return
	}

	w.WriteHeader(http.StatusOK)
}
