package router

import (
	"net/http"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
)

func newAccessCookie(token string) *http.Cookie {
	return &http.Cookie{
		Name:     auth.AccessKey,
		Value:    token,
		Path:     "/",
		Expires:  time.Now().UTC().Add(auth.AccessTime),
		Secure:   false,
		HttpOnly: true,
	}
}

func newRefreshCookie(token string) *http.Cookie {
	return &http.Cookie{
		Name:     auth.RefreshKey,
		Value:    token,
		Path:     "/",
		Expires:  time.Now().UTC().Add(auth.RefreshTime),
		Secure:   false,
		HttpOnly: true,
	}
}

func resetCookie(name string) *http.Cookie {
	return &http.Cookie{
		Name:     name,
		Path:     "/",
		Expires:  time.Now().UTC(),
		Secure:   false,
		HttpOnly: true,
	}
}
