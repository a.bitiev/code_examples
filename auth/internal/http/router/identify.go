package router

import (
	"net/http"

	"github.com/go-chi/render"
)

type UserInfo struct {
	Login string `json:"login"`
}

func (u *UserRouter) identify(w http.ResponseWriter, r *http.Request) {
	user, err := getUserCtx(r)
	if err != nil {
		handleError(w, r, err)
		return
	}

	resp := &UserInfo{
		Login: user.Login,
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, resp)
}

func (u *UserRouter) me(w http.ResponseWriter, r *http.Request) {
	user, err := getUserCtx(r)
	if err != nil {
		handleError(w, r, err)
		return
	}

	resp := &UserInfo{
		Login: user.Login,
	}

	render.Status(r, http.StatusOK)
	render.JSON(w, r, resp)
}
