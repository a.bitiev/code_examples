package router

import (
	"errors"
	"net/http"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/errs"

	"github.com/go-chi/render"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
)

const internal = "internal error"

type Error struct {
	Code  int    `json:"code"`
	Error string `json:"error"`
}

func handleError(w http.ResponseWriter, r *http.Request, err error) {
	var (
		code int
		msg  string
	)

	switch {
	case
		errors.Is(err, errs.ErrBadRequest):
		code = http.StatusBadRequest
		msg = err.Error()

	case
		errors.Is(err, errs.ErrAlreadyExists),
		errors.Is(err, errs.ErrUnprocessableEntity):
		code = http.StatusUnprocessableEntity
		msg = err.Error()

	case
		errors.Is(err, errs.ErrNotFound):
		code = http.StatusNotFound
		msg = err.Error()

	case
		errors.Is(err, auth.ErrUnauthorized),
		errors.Is(err, errs.ErrInvalidPassword):
		code = http.StatusUnauthorized
		msg = err.Error()

	default:
		code = http.StatusInternalServerError
		msg = internal
	}

	render.Status(r, code)
	render.JSON(w, r, &Error{
		Code:  code,
		Error: msg,
	})
}
