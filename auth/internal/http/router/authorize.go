package router

import (
	"context"
	"net/http"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/auth"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

func (u *UserRouter) authorize(w http.ResponseWriter, user *users.User) error {
	access, err := u.auth.NewToken(user, auth.AccessTime)
	if err != nil {
		return err
	}

	refresh, err := u.auth.NewToken(user, auth.RefreshTime)
	if err != nil {
		return err
	}

	http.SetCookie(w, newAccessCookie(access))
	http.SetCookie(w, newRefreshCookie(refresh))

	return nil
}

func (u *UserRouter) reauthorize(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user, hasAccess := u.authenticate(r, auth.AccessKey)
		if hasAccess {
			r = r.WithContext(context.WithValue(r.Context(), auth.UserKey, user))
			next.ServeHTTP(w, r)
			return
		}

		user, hasRefresh := u.authenticate(r, auth.RefreshKey)
		if !hasRefresh {
			handleError(w, r, auth.ErrUnauthorized)
			return
		}

		if err := u.authorize(w, user); err != nil {
			handleError(w, r, err)
			return
		}

		r = r.WithContext(context.WithValue(r.Context(), auth.UserKey, user))

		next.ServeHTTP(w, r)
	})
}
