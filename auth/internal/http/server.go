package http

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/authorization"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/http/router"
)

const (
	readTimeout  = 15 * time.Second
	writeTimeout = 15 * time.Second
)

type Server struct {
	server            *http.Server
	certFile, keyFile string
}

func New(cfg *config.Config, app *authorization.Authorization) (*Server, error) {
	r, err := router.NewClientRouter(cfg, app)
	if err != nil {
		return nil, err
	}

	return &Server{
		server: &http.Server{
			Addr:         cfg.HTTP.Addr,
			Handler:      r.Routes(),
			ReadTimeout:  readTimeout,
			WriteTimeout: writeTimeout,
		},
		certFile: cfg.HTTP.CertFile,
		keyFile:  cfg.HTTP.KeyFile,
	}, nil
}

func (s *Server) Run() error {
	var err error

	if s.certFile == "" || s.keyFile == "" {
		err = s.server.ListenAndServe()
	} else {
		err = s.server.ListenAndServeTLS(s.certFile, s.keyFile)
	}

	if err != nil && err != http.ErrServerClosed {
		return err
	}

	return nil
}

func (s *Server) Stop() error {
	return s.server.Shutdown(context.TODO())
}
