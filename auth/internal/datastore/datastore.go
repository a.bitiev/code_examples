package datastore

import (
	"context"
	"errors"
	"fmt"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/datastore/memory"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/datastore/mongodb"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

var ErrInvalidDatastoreEngine = errors.New("invalid datastore engine")

type Datastore interface {
	base
	UserRepo
}

type base interface {
	Ping(ctx context.Context) error
	Close() error
}

type UserRepo interface {
	CreateUser(ctx context.Context, user *users.User) (string, error)
	UserByID(ctx context.Context, id string) (*users.User, error)
	UserByName(ctx context.Context, username string) (*users.User, error)
	UpdateUser(ctx context.Context, user *users.User) error
	DeleteUser(ctx context.Context, id string) error
}

func New(cfg *config.Config) (Datastore, error) {
	switch cfg.Datastore.Driver {
	case memory.Driver:
		return memory.New(cfg)
	case mongodb.Driver:
		return mongodb.New(cfg)
	default:
		return nil, fmt.Errorf("%w: %s", ErrInvalidDatastoreEngine, cfg.Datastore.Driver)
	}
}
