package memory

import (
	"context"
	"errors"
	"sync"

	"github.com/google/uuid"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

const Driver = "memory"

type Memory struct {
	mux   sync.RWMutex
	store map[string]*users.User
}

func New(cfg *config.Config) (*Memory, error) {
	mem := &Memory{
		store: make(map[string]*users.User, 1),
	}

	admin := &users.User{
		Login:    cfg.Admin.Login,
		Password: cfg.Admin.Password,
	}

	err := mem.Ping(context.TODO())
	if err != nil {
		return nil, err
	}

	_, err = mem.CreateUser(context.TODO(), admin)
	if err != nil {
		return nil, err
	}

	return mem, nil
}

func (m *Memory) Ping(ctx context.Context) error {
	if m == nil || m.store == nil {
		return errors.New("memory not initialized")
	}

	return nil
}

func (m *Memory) Close() error {
	return nil
}

func (m *Memory) CreateUser(ctx context.Context, user *users.User) (string, error) {
	user.ID = uuid.New().String()

	m.mux.Lock()
	m.store[user.ID] = user
	m.mux.Unlock()

	return user.ID, nil
}

func (m *Memory) UserByID(ctx context.Context, id string) (*users.User, error) {
	m.mux.RLock()
	user, exists := m.store[id]
	m.mux.RUnlock()

	if !exists {
		return nil, nil
	}

	return user, nil
}

func (m *Memory) UserByName(ctx context.Context, username string) (*users.User, error) {
	m.mux.RLock()
	for _, user := range m.store {
		if username == user.Login {
			return user, nil
		}
	}
	m.mux.RUnlock()

	return nil, nil
}

func (m *Memory) UpdateUser(ctx context.Context, user *users.User) error {
	old, err := m.UserByID(ctx, user.ID)
	if err != nil {
		return err
	}

	m.mux.Lock()
	old.Login = user.Login
	old.Password = user.Password
	m.mux.Unlock()

	return nil
}

func (m *Memory) DeleteUser(ctx context.Context, id string) error {
	_, err := m.UserByID(ctx, id)
	if err != nil {
		return err
	}

	m.mux.Lock()
	delete(m.store, id)
	m.mux.Unlock()

	return nil
}
