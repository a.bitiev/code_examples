package mongodb

import (
	"context"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const (
	Driver            = "mongodb"
	userCollection    = "users"
	MongodbDefaultURI = "mongodb://localhost:27017/"
)

type Mongodb struct {
	client *mongo.Client
	users  *mongo.Collection
}

func New(cfg *config.Config) (*Mongodb, error) {
	ctx, cancel := context.WithTimeout(context.Background(), cfg.Datastore.Timeout)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(cfg.Datastore.URL))
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, err
	}

	db := client.Database(cfg.Datastore.Name)

	mdb := &Mongodb{
		client: client,
		users:  db.Collection(userCollection),
	}

	_, err = mdb.AddUserIfNotExist(ctx, cfg.Admin.Login, cfg.Admin.Password)
	if err != nil {
		return nil, err
	}

	return mdb, nil
}

func (m *Mongodb) Close() error {
	return m.client.Disconnect(context.TODO())
}
