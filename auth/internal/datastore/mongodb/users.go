package mongodb

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/errs"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type User struct {
	ObjectID primitive.ObjectID `bson:"_id"`
	Login    string             `bson:"login"`
	Password string             `bson:"password"`
}

func (m *Mongodb) CreateUser(ctx context.Context, user *users.User) (string, error) {
	inserted, err := m.users.InsertOne(ctx, user.CreateDocument())

	if mongo.IsDuplicateKeyError(err) {
		return "", fmt.Errorf("user with login %s %w", user.Login, errs.ErrAlreadyExists)
	}

	if err != nil {
		return "", err
	}

	id, ok := inserted.InsertedID.(primitive.ObjectID)
	if !ok {
		return "", fmt.Errorf("unexpected type of user id %T", inserted.InsertedID)
	}

	return id.Hex(), nil
}

func (m *Mongodb) UserByID(ctx context.Context, userID string) (*users.User, error) {
	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, fmt.Errorf("%w: invalid user id", errs.ErrBadRequest)
	}

	user := new(User)

	err = m.users.FindOne(ctx, bson.M{"_id": id}).Decode(user)

	if err == mongo.ErrNoDocuments {
		return nil, fmt.Errorf("user with id %s %w", userID, errs.ErrNotFound)
	}

	if err != nil {
		return nil, err
	}

	return &users.User{
		ID:       user.ObjectID.Hex(),
		Login:    user.Login,
		Password: user.Password,
	}, nil
}

func (m *Mongodb) UserByName(ctx context.Context, login string) (*users.User, error) {
	user := new(User)

	err := m.users.FindOne(ctx, bson.M{"login": login}).Decode(user)

	if err == mongo.ErrNoDocuments {
		return nil, fmt.Errorf("user with login %s %w", login, errs.ErrNotFound)
	}

	if err != nil {
		return nil, err
	}

	return &users.User{
		ID:       user.ObjectID.Hex(),
		Login:    user.Login,
		Password: user.Password,
	}, nil
}

func (m *Mongodb) UpdateUser(ctx context.Context, user *users.User) error {
	id, err := primitive.ObjectIDFromHex(user.ID)
	if err != nil {
		return fmt.Errorf("%w: invalid user id", errs.ErrBadRequest)
	}

	updated, err := m.users.UpdateOne(ctx, bson.M{"_id": id}, user.UpdateDocument())
	if err != nil {
		return err
	}

	if updated.MatchedCount == 0 || updated.ModifiedCount == 0 {
		return fmt.Errorf("user with id %s %w", user.ID, errs.ErrNotFound)
	}

	return nil
}

func (m *Mongodb) DeleteUser(ctx context.Context, userID string) error {
	id, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return fmt.Errorf("%w: invalid user id", errs.ErrBadRequest)
	}

	deleted, err := m.users.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return err
	}

	if deleted.DeletedCount == 0 {
		return fmt.Errorf("user with id %s %w", userID, errs.ErrNotFound)
	}

	return nil
}

func (m *Mongodb) Ping(ctx context.Context) error {
	if m == nil || m.client == nil {
		return fmt.Errorf("mongodb not initialized")
	}

	return m.client.Ping(ctx, readpref.Primary())
}

func (m *Mongodb) AddUserIfNotExist(ctx context.Context, login string, password string) (*users.User, error) {
	user, err := m.UserByName(ctx, login)
	if err != nil {
		userId, err := m.CreateUser(ctx, &users.User{
			Login:    login,
			Password: password,
		})
		if err != nil {
			return nil, err
		}
		user, err = m.UserByID(ctx, userId)
		if err != nil {
			return nil, err
		}
	}
	return user, nil
}
