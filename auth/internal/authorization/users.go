package authorization

import (
	"context"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/errs"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

func (a *Authorization) CreateUser(ctx context.Context, user *users.User) (string, error) {
	err := Validate(user)
	if err != nil {
		return "", err
	}

	encrypted, err := a.encryptor.Hash(user.Password)
	if err != nil {
		return "", err
	}

	user.Password = encrypted

	return a.store.CreateUser(ctx, user)
}

func (a *Authorization) UserByID(ctx context.Context, id string) (*users.User, error) {
	if id == "" {
		return nil, errs.ErrUnprocessableEntity
	}

	user, err := a.store.UserByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (a *Authorization) UpdateUser(ctx context.Context, user *users.User) error {
	err := Validate(user)
	if err != nil {
		return err
	}

	encrypted, err := a.encryptor.Hash(user.Password)
	if err != nil {
		return err
	}

	user.Password = encrypted

	return a.store.UpdateUser(ctx, user)
}

func (a *Authorization) DeleteUser(ctx context.Context, id string) error {
	if id == "" {
		return errs.ErrUnprocessableEntity
	}

	return a.store.DeleteUser(ctx, id)
}
