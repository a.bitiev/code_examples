package authorization

import (
	"fmt"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/errs"
)

type Validatable interface {
	Validate() error
}

func Validate(v Validatable) error {
	if err := v.Validate(); err != nil {
		return fmt.Errorf("%w: %v", errs.ErrUnprocessableEntity, err)
	}

	return nil
}
