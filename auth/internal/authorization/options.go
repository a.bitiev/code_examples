package authorization

import (
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/datastore"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/secure"
)

type Option func(auth *Authorization)

func WithDatastore(cfg *config.Config) (Option, error) {
	store, err := datastore.New(cfg)
	if err != nil {
		return nil, err
	}

	return func(auth *Authorization) {
		auth.store = store
	}, nil
}

func WithPasswordEncryptor(cfg *config.Config) (Option, error) {
	encryptor, err := secure.NewPasswordEncryptor(cfg.Secure.HashAlgorithm)
	if err != nil {
		return nil, err
	}

	return func(auth *Authorization) {
		auth.encryptor = encryptor
	}, nil
}
