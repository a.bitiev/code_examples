package authorization

import (
	"context"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/errs"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/pkg/users"
)

func (a *Authorization) Authorize(ctx context.Context, input *users.User) (*users.User, error) {
	err := Validate(input)
	if err != nil {
		return nil, err
	}

	user, err := a.store.UserByName(ctx, input.Login)
	if err != nil {
		return nil, err
	}

	if !a.encryptor.Compare(user.Password, input.Password) {
		return nil, errs.ErrInvalidPassword
	}

	return user, nil
}
