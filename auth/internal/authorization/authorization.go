package authorization

import (
	"log"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/datastore"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/secure"
)

const Name = "authorization"

type Authorization struct {
	store     datastore.Datastore
	encryptor secure.PasswordEncryptor
}

func New(cfg *config.Config) (*Authorization, error) {
	auth := new(Authorization)
	options := []func(*config.Config) (Option, error){
		WithDatastore,
		WithPasswordEncryptor,
	}

	var (
		apply Option
		err   error
	)

	for _, option := range options {
		apply, err = option(cfg)
		if err != nil {
			return nil, err
		}

		apply(auth)
	}

	return auth, nil
}

func (a *Authorization) Name() string {
	return Name
}

type Closer interface {
	Close() error
}

func (a *Authorization) Stop() {
	var err error

	internals := []Closer{
		a.store,
	}

	for _, internal := range internals {
		if err = internal.Close(); err != nil {
			log.Println(err)
		}
	}
}
