package bcrypt

import "golang.org/x/crypto/bcrypt"

const Algorithm = "bcrypt"

type Encryptor struct{}

func New() (*Encryptor, error) {
	return &Encryptor{}, nil
}

func (b *Encryptor) Hash(password string) (string, error) {
	encrypted, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(encrypted), nil
}

func (b *Encryptor) Compare(hashed, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hashed), []byte(password)) == nil
}
