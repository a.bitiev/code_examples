package secure

import (
	"fmt"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/secure/bcrypt"
)

type PasswordEncryptor interface {
	Hash(password string) (string, error)
	Compare(hashed, password string) bool
}

func NewPasswordEncryptor(algo string) (PasswordEncryptor, error) {
	switch algo {
	case bcrypt.Algorithm:
		return bcrypt.New()
	default:
		return nil, fmt.Errorf("invalid password hashing algorithm: %s", algo)
	}
}
