package service

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"

	"gitlab.com/golang-developer-cloudmts_main/team8/auth/config"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/authorization"
	"gitlab.com/golang-developer-cloudmts_main/team8/auth/internal/http"
)

type Service struct {
	app  *authorization.Authorization
	http *http.Server
}

func New(cfg *config.Config) (*Service, error) {
	app, err := authorization.New(cfg)
	if err != nil {
		return nil, fmt.Errorf("%s service init error: %w", app.Name(), err)
	}

	httpServer, err := http.New(cfg, app)
	if err != nil {
		return nil, fmt.Errorf("%s http server init error: %w", app.Name(), err)
	}

	return &Service{
		app:  app,
		http: httpServer,
	}, nil
}

func (s *Service) Run() {
	log.Printf("%s service started", s.app.Name())

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go s.handleSignal(ctx, cancel)

	go func() {
		log.Println("http server running...")
		if err := s.http.Run(); err != nil {
			log.Printf("failed to run http server: %v", err)
			cancel()
		}
	}()

	<-ctx.Done()

	s.gracefulShutdown()

	log.Printf("%s service stopped", s.app.Name())
}

func (s *Service) gracefulShutdown() {
	var err error

	if err = s.http.Stop(); err != nil {
		log.Printf("http server shutdown: %v", err)
	} else {
		log.Println("http server shutdown: success")
	}

	s.app.Stop()
}

func (s *Service) handleSignal(ctx context.Context, cancel context.CancelFunc) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)

	select {
	case <-sigint:
		log.Println("received SIGINT, shutting down...")
		cancel()
	case <-ctx.Done():
		return
	}
}
