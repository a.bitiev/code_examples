package config

import (
	"github.com/spf13/viper"
	"time"
)

type Config struct {
	Datastore *Datastore
	Secure    *Secure
	HTTP      *HTTP
	Admin     *Admin
}

type Datastore struct {
	Driver  string
	URL     string
	Name    string
	Timeout time.Duration
}

type Secure struct {
	Method        string
	SecretKey     string
	HashAlgorithm string
}

type HTTP struct {
	Addr     string
	CertFile string
	KeyFile  string
}

type Admin struct {
	Login    string
	Password string
}

func Parse() (*Config, error) {
	viper.AutomaticEnv()

	viper.SetDefault("datastore_driver", "mongodb")
	viper.SetDefault("datastore_url", "mongodb://localhost:27017/")
	viper.SetDefault("datastore_name", "auth")
	viper.SetDefault("datastore_timeout", "5s")

	viper.SetDefault("secure_method", "jwt")
	viper.SetDefault("secure_secret_key", "secret")
	viper.SetDefault("secure_hash_algorithm", "bcrypt")

	viper.SetDefault("http_addr", ":8080")
	viper.SetDefault("http_cert_file", "")
	viper.SetDefault("http_key_file", "")

	viper.SetDefault("admin_login", "admin")
	viper.SetDefault("admin_password", "$2a$12$QrrYtLQhdi14XyPjz30r2.WNT7uG5E3AL5bm2ObQuC/6DVsVTgtHu")

	return &Config{
		Datastore: &Datastore{
			Driver:  viper.GetString("datastore_driver"),
			URL:     viper.GetString("datastore_url"),
			Name:    viper.GetString("datastore_name"),
			Timeout: viper.GetDuration("datastore_timeout"),
		},
		Secure: &Secure{
			Method:        viper.GetString("secure_method"),
			SecretKey:     viper.GetString("secure_secret_key"),
			HashAlgorithm: viper.GetString("secure_hash_algorithm"),
		},
		HTTP: &HTTP{
			Addr:     viper.GetString("http_addr"),
			CertFile: viper.GetString("http_cert_file"),
			KeyFile:  viper.GetString("http_key_file"),
		},
		Admin: &Admin{
			Login:    viper.GetString("admin_login"),
			Password: viper.GetString("admin_password"),
		},
	}, nil
}
